FROM ruby

EXPOSE 3001

ADD . /code
WORKDIR /code

RUN apt-get update && \
    apt-get install nodejs -y

RUN gem install bundle && \
    bundle install

RUN rails db:create && \
    rails db:migrate

CMD ["rails", "s", "-b", "0.0.0.0", "-p",  "3001"]
