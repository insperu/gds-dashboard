module EndpointConcern
  extend ActiveSupport::Concern

  def decode_as_json(endpoint)
    json = ActiveSupport::JSON.decode(open(get_url + endpoint).read)
    json["data"]
  end

  def get_url
    api_host=ENV["GDS_API_HOST"] || "localhost"
    api_port=ENV["GDS_API_PORT"] || "80"
    "http://#{api_host}:#{api_port}/"
  end
end
