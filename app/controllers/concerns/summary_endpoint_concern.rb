module SummaryEndpointConcern
  include EndpointConcern

  def get_summary
    decode_as_json("dashboard/summary")
  end
end
