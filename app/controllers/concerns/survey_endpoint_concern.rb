module SurveyEndpointConcern
  include EndpointConcern

  def get_surveys
    decode_as_json("dashboard/surveys")
  end
end
