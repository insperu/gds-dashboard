module DiseaseEndpointConcern
  include EndpointConcern

  def get_diseases
    decode_as_json("dashboard/diseases")
  end
end
